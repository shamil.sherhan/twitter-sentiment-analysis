# Twitter Sentiment Analysis



Sentiment Analysis is the process of computationally identifying and categorizing opinions expressed in a piece of text, especially in order to determine whether the writer's attitude towards a particular topic, product, etc. is positive, negative, or neutral.

The tweets made by Donald Trump since his start of presidency from Jan 20 2017 to June 21 2018, are analysed using LSTM, NLTK, Keras.
